package com.social.solution.feature_share_impl.bottom_sheet

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.social.solution.feature_share_impl.MainObject
import com.social.solution.feature_share_impl.MainObject.TAG
import com.social.solution.feature_share_impl.R
import com.social.solution.feature_share_impl.bottom_sheet_menu.MenuFragment
import com.social.solution.feature_share_module_api.models.ChannelUI
import kotlinx.android.synthetic.main.share_bottom_sheet.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class BottomSheetFragment(
    private val fragmentMenu: MenuFragment,
    private val visibilityMenu: Boolean
) : Fragment() {

    companion object {
        const val ID = "ShareBottomSheet_TAG"
    }

    private lateinit var feedBSViewModel: FeedBSViewModel
    private lateinit var adapter: FeedBSChannelAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        feedBSViewModel = ViewModelProviders.of(this).get(FeedBSViewModel::class.java)
        return inflater.inflate(R.layout.share_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        MainObject.mCallback!!.onChangeState(bottomSheet, BottomSheetBehavior.STATE_COLLAPSED)
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                MainObject.mCallback!!.onChangeState(bottomSheet, newState)
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        setBackgroundRoundExpanded(false)
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        setBackgroundRoundExpanded()
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    else -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (visibilityMenu)
                    if (slideOffset <= -0.1f) {
                        fragmentMenu.view!!.visibility = View.GONE
                    } else {
                        fragmentMenu.view!!.visibility = View.VISIBLE
                    }
            }
        })

        bottomSheet.post {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        initTopChannel()
    }

    private fun initTopChannel() {
        GlobalScope.launch(Dispatchers.Main) {
            var channelList = listOf<ChannelUI>()
            adapter = withContext(Dispatchers.IO) {
                FeedBSChannelAdapter(
                    channelList,
                    object :
                        FeedBSChannelAdapter.BSChannelCallbacks {
                        override fun selectChannel(userId: String?) {
//                            feedBSViewModel.sendToChannel(userId, post) TODO
                            Toast.makeText(context, "SEND POST", Toast.LENGTH_SHORT).show()
                        }

                        override fun callBackSearch(str: String) {
//                            mCallback.onTextChanged(str) TODO
                        }
                    })
            }
            feedBSViewModel.loadChannel(MainObject.mBackendApi)
            feedBSViewModel.getTopChannels()
                .observe(viewLifecycleOwner, Observer<List<ChannelUI>> { channels ->
                    Log.d(TAG, "OBSERVE + $channels")
                    channelList = channels
                    adapter.channels = channelList
                    adapter.notifyDataSetChanged()
                })
            val channelDividerDecorator = withContext(Dispatchers.IO) {
                object : DividerItemDecoration(context, VERTICAL) {
                    override fun getItemOffsets(
                        outRect: Rect,
                        view: View,
                        parent: RecyclerView,
                        state: RecyclerView.State
                    ) {
                        val position: Int = parent.getChildAdapterPosition(view)
                        if (position == 0) {
                            outRect.set(0, 100, 0, 0)
                        } else {
                            if (position == state.itemCount - 1)
                                outRect.set(0, 0, 0, 450)
                        }
                    }
                }
            }
            recyclerItems.addItemDecoration(channelDividerDecorator)
            commitAdapter()
        }
    }

    private fun commitAdapter() {
        recyclerItems.adapter = adapter
        recyclerItems.layoutManager = LinearLayoutManager(context)
        adapter.notifyDataSetChanged()
    }

    private fun setBackgroundRoundExpanded(bool: Boolean = true) {
        if (bool) {
            view_expand.visibility = View.INVISIBLE
        } else {
            view_expand.visibility = View.VISIBLE
        }
    }
}