package com.social.solution.feature_share_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_share_module_api.FeatureShareApi

object LibShareDependency {

    fun featureShareApi(context: Context, backend: FeatureBackendApi): FeatureShareApi {
        return FeatureShareImpl(ShareStarterImpl(), context, backend)
    }
}