package com.social.solution.feature_share_impl

import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_share_impl.bottom_sheet.BottomSheetFragment
import com.social.solution.feature_share_impl.bottom_sheet_menu.MenuFragment
import com.social.solution.feature_share_module_api.FeatureShareCallback
import com.social.solution.feature_share_module_api.FeatureShareStarter

class ShareStarterImpl: FeatureShareStarter {

    override fun start(
        manager: FragmentManager,
        container: Int,
        postId: String,
        callback: FeatureShareCallback,
        featureBackendApi: FeatureBackendApi,
        visibilityMenu: Boolean
    ) {
        MainObject.mCallback = callback
        MainObject.mBackendApi = featureBackendApi

        val fragmentMenu = MenuFragment(visibilityMenu)

        val fragment = BottomSheetFragment(fragmentMenu, visibilityMenu)
        manager.beginTransaction()
            .add(container, fragment)
            .addToBackStack(BottomSheetFragment.ID)
            .commit()
        manager.beginTransaction()
            .add(container, fragmentMenu)
            .addToBackStack(MenuFragment.TAG)
            .commit()
    }
}