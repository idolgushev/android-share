package com.social.solution.feature_share_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_share_module_api.FeatureShareApi
import com.social.solution.feature_share_module_api.FeatureShareStarter

class FeatureShareImpl(
    private val starter: FeatureShareStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureShareApi {
    override fun shareStarter(): FeatureShareStarter = starter

}