package com.social.solution.feature_share_impl

import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_share_module_api.FeatureShareCallback

object MainObject {

    var mCallback: FeatureShareCallback? = null
    var mBackendApi: FeatureBackendApi? = null
    var TAG = "SHARE_MODULE"
}