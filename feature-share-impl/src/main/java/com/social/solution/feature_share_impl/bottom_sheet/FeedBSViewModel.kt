package com.social.solution.feature_share_impl.bottom_sheet

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feedMemus.modelRequest.GetChannelsRequest
import com.social.solution.feature_share_impl.MainObject.TAG
import com.social.solution.feature_share_impl.utils.convertFrom
import com.social.solution.feature_share_module_api.models.ChannelUI

class FeedBSViewModel : ViewModel() {


    private val channels = MutableLiveData<List<ChannelUI>>()


    fun getTopChannels(): LiveData<List<ChannelUI>> {
        return channels
    }


    @SuppressLint("CheckResult")
    fun loadChannel(backendApi: FeatureBackendApi?) {
        val getChannelsRequest = GetChannelsRequest(0, 10)
        backendApi?.feedMemusApi()?.getChats(getChannelsRequest)?.subscribe({ result ->
            channels.value = result?.channels?.map {
                convertFrom(
                    it
                )
            }
            Log.d(TAG, channels.value.toString())
        }, { throwable ->
            Log.e(TAG, throwable.printStackTrace().toString())
        })
    }
}
