package com.social.solution.feature_share_impl.bottom_sheet_menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.social.solution.feature_share_impl.R


class MenuFragment(private val visibilityMenu: Boolean) : Fragment() {

    companion object {
        const val TAG = "ShareBottomSheet_TAG"
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.share_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setVisibilityMenu()
    }
    private fun setVisibilityMenu() {
        if (visibilityMenu) {
            view?.visibility = View.VISIBLE
        } else {
            view?.visibility = View.GONE
        }
    }
}