package com.social.solution.feature_share_impl.utils

import com.morozov.core_backend_api.feedMemus.model.Channel
import com.social.solution.feature_share_module_api.models.ChannelUI

fun convertFrom(channel: Channel): ChannelUI {
    return ChannelUI(
        channel.userId,
        channel.userName,
        channel.avatar,
        channel.dateActive
    )
}