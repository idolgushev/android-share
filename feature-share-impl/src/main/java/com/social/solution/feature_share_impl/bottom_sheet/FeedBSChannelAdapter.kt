package com.social.solution.feature_share_impl.bottom_sheet

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.social.solution.feature_share_impl.MainObject.TAG
import com.social.solution.feature_share_impl.R
import com.social.solution.feature_share_module_api.models.ChannelUI
import kotlinx.android.synthetic.main.share_bottom_sheet_card_search.view.*
import kotlinx.android.synthetic.main.share_bottom_sheet_card_channel.view.*

const val SEARCH_TYPE = 0
const val CHANNEL_TYPE = 1

class FeedBSChannelAdapter(
    var channels: List<ChannelUI>,
    private val callbacks: BSChannelCallbacks
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CHANNEL_TYPE) {
            ChannelViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.share_bottom_sheet_card_channel,
                    parent,
                    false
                )
            )
        } else {
            SearchViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.share_bottom_sheet_card_search,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = channels.size

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            SEARCH_TYPE
        } else {
            CHANNEL_TYPE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0)
            (holder as SearchViewHolder).onBind(
                callbacks
            )
        else
            (holder as ChannelViewHolder).onBind(
                position, channels, callbacks
            )

    }

    class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(
            callbacks: BSChannelCallbacks
        ) {
            itemView.feed_share_edit_text_search.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    callbacks.callBackSearch(str.toString())
                }
            })
        }
    }


    class ChannelViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(
            position: Int,
            channelList: List<ChannelUI>,
            callbacks: BSChannelCallbacks
        ) {
            val chatUi = channelList[position]
            Glide
                .with(itemView.icon_channel)
                .asDrawable()
                .placeholder(R.drawable.share_channel_placeholder_ic)
                .load(chatUi.avatar)
                .into(itemView.icon_channel)
            itemView.name_channel.text = chatUi.userName
            itemView.setOnClickListener {
                callbacks.selectChannel(chatUi.userId)
            }
        }
    }

    interface BSChannelCallbacks {
        fun selectChannel(userId: String?)

        fun callBackSearch(str: String)
    }

}
