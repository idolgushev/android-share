package com.social.solution.android_share

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_share_impl.LibShareDependency
import com.social.solution.feature_share_module_api.FeatureShareCallback
import kotlinx.android.synthetic.main.bottom_sheet_container.*

class MainActivity : AppCompatActivity() {
    private lateinit var featureBackendApi: FeatureBackendApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        featureBackendApi = LibBackendDependency.featureBackendApi(applicationContext)
        startBottomNavigation("testId")
    }

    private fun startBottomNavigation(postId: String) {
        val featureShareApi =
            LibShareDependency.featureShareApi(applicationContext, featureBackendApi)
        featureShareApi.shareStarter().start(
            supportFragmentManager,
            R.id.main_container_bottom_sheet,
            postId,
            ShareCallback(),
            featureBackendApi,
            visibilityMenu = true
        )
    }

    inner class ShareCallback : FeatureShareCallback {
        override fun sendToChannel(isSended: Boolean, channelId: Int) {
        }

        override fun addToFavorite(postID: Int) {
        }

        override fun addTemplate(postID: Int) {
        }

        override fun copyLink(link: String, isCopiedToBuffer: Boolean) {
        }

        override fun more() {
        }

        override fun onChangeState(bottomSheet: View, newState: Int) {
            if (newState != BottomSheetBehavior.STATE_HIDDEN && newState != BottomSheetBehavior.STATE_EXPANDED) {
                view_back.visibility = View.VISIBLE
                view_back.alpha = 0.75f
            } else {
                println("HIDDEN_VIEW")
                view_back.visibility = View.GONE
                view_back.alpha = 1.0f
            }
        }
    }
}
