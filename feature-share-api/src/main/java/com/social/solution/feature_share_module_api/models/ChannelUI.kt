package com.social.solution.feature_share_module_api.models

data class ChannelUI(
    val userId: String?,
    val userName: String?,
    val avatar: String,
    val dateActive: String?
)