package com.social.solution.feature_share_module_api

import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.FeatureBackendApi

interface FeatureShareStarter {

    fun start(
        manager: FragmentManager,
        container: Int,
        postId: String,
        callback: FeatureShareCallback,
        featureBackendApi: FeatureBackendApi,
        visibilityMenu: Boolean
    )
}
