package com.social.solution.feature_share_module_api

interface FeatureShareApi {
    fun shareStarter(): FeatureShareStarter
}