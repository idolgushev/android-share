package com.social.solution.feature_share_module_api

import android.view.View


interface FeatureShareCallback {
    fun sendToChannel(isSended: Boolean, channelId: Int)
    fun addToFavorite(postID: Int)
    fun addTemplate(postID: Int)
    fun copyLink(link: String, isCopiedToBuffer: Boolean)
    fun more()
    fun onChangeState(bottomSheet: View, newState: Int)
}